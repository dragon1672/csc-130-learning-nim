﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim.Players
{
    public class Human : Player
    {

        private bool playerHasGivenInput(out int input)
        {
            return (Int32.TryParse(Console.ReadLine(), out input));
        }

        private int acceptNumericalInput(int lowerBound, int upperBound)
        {
            Console.WriteLine("Please input a number between " + lowerBound + " and " + upperBound + ".\n");
            int retval = 0;

            bool valueInput = false;
            do
            {
                if (playerHasGivenInput(out retval))
                {
                    valueInput = (retval >= lowerBound && retval <= upperBound);
                    if (!valueInput)
                    {
                        Console.WriteLine("Please input a number between "+lowerBound+" and "+upperBound+".\n");
                    }
                }
                else
                {
                    Console.WriteLine("Please input a number.\n");
                }
            } while (!valueInput);

            return retval;
        }

        public override MoveData GetMove(Interfaces.IGameInstance instance)
        {
            
            int NUMBER_OF_ROWS = instance.CurrentState.Data.Length;

            Console.WriteLine(this.Name + "'s turn.\n Which row would you like to modify?\n" + instance.CurrentState.ToStickString());
            
            int sticksLeftInRow = 0;
            int rowModified = 0;
            do
            {
                rowModified = acceptNumericalInput(1, NUMBER_OF_ROWS) - 1;
                sticksLeftInRow = instance.CurrentState.Data[rowModified];

                if (sticksLeftInRow == 0)
                {
                    Console.WriteLine("\nThat row has no sticks left.");
                }
            } while (sticksLeftInRow == 0);


            Console.WriteLine("How many sticks would you like to remove?\n");
            int sticksRemoved = acceptNumericalInput(1, sticksLeftInRow);

            if (sticksLeftInRow < sticksRemoved)
            {
                sticksRemoved = sticksLeftInRow;
            }
            
            return new MoveData(rowModified, sticksRemoved);
        }
    }
}