﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearninNim.Interfaces;

namespace LearninNim.Players
{
    class AIRandom:Player
    {
        public override MoveData GetMove(Interfaces.IGameInstance instance)
        {
            List<MoveData> possibleMoves = new List<MoveData>();
			foreach (MoveData current in GameState.getPossibleMoves(instance.CurrentState))
			{
				possibleMoves.Add(current);
			}
            int randomIndex = new Random().Next() % possibleMoves.Count();
            return possibleMoves[randomIndex];
        }
    }
}
