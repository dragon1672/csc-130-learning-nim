﻿using LearninNim.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim.Players
{
    public class AI : Player
    {
		static protected Random rand = new Random();
        protected IScoreTable meBrain;
        
		public AI(IScoreTable brain)
        {
            meBrain = brain;
        }
		
		protected MoveData getLowestRankingMove(GameState currentState)
		{
			List<MoveData> lowestRankingMoves = new List<MoveData>();
			double? currentScore = null;////score is cached to increase preformance

			foreach (MoveData newGuyState in GameState.getPossibleMoves(currentState))
			{
				double newGuyScore = meBrain.GetScoreForState(currentState.Move2State(newGuyState));
				if (currentScore > newGuyScore || currentScore == null)
				{
					lowestRankingMoves.Clear();
					lowestRankingMoves.Add(newGuyState);
					currentScore = newGuyScore;
				}
				else if (currentScore == newGuyScore)
				{
					lowestRankingMoves.Add(newGuyState);
				}
			}

			int index = rand.Next(lowestRankingMoves.Count);

			return lowestRankingMoves[index];
		}
        // override
        public override MoveData GetMove(Interfaces.IGameInstance instance)
        {
			return getLowestRankingMove(instance.CurrentState);
        }
    }
}
