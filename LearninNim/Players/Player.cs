﻿using LearninNim.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim.Players
{
    public abstract class Player
    {
        public string Name { get; set; }

		public abstract MoveData GetMove(IGameInstance instance);
    }
}
