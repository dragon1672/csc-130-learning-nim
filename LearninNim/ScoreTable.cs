﻿using LearninNim.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearninNim.Players;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearninNim
{
    class ScoreTable : IScoreTable
    {
        private const int EmptyListLength = 0;
        private const int LoopStartIndex = 0;
        private const int InitialFrequency = 1;
        private const int LoserScorePositivity = -1;
        private const int WinnerScorePositivity = +1;
        private const double UnknownStateScore = 0.0;
        private const string DisplayFormatString = "{0} | {1}\n";

        private IDictionary<GameState, ScoreTableData> mTable;

        public ScoreTable()
        {
            mTable = new Dictionary<GameState, ScoreTableData>();
        }

        /// <summary>
        /// Updates this ScoreTable's internal list according to the given winner and game history
        /// </summary>
        /// <param name="winner">The player that won the game associated with the given game log</param>
        /// <param name="gameLog">A list of tuples pairing a player with a received game state</param>
        /// <remarks>
        /// Precondition: gameLog is ordered by actual gameplay order
        /// </remarks>
        public void UpdateTable(Player winner, IList<GameLogData> gameLog)
        {
            Assert.IsNotNull(winner);
            Assert.IsNotNull(gameLog);
            Assert.IsTrue(gameLog.Count > EmptyListLength);

            var winnerStates = (from data in gameLog
                                where data.Player == winner
                                select data.Move).ToList();

            var loserStates = (from data in gameLog
                               where data.Player != winner
                               select data.Move).ToList();

            Assert.IsTrue(winnerStates.Count > EmptyListLength);
            Assert.IsTrue(loserStates.Count > EmptyListLength);

            UpdateTableFromStateList(winnerStates, isWinner: true);
            UpdateTableFromStateList(loserStates, isWinner: false);
        }

        /// <summary>
        /// Updates the table according to a givens state list and whether or not the list contains winning or losing states
        /// </summary>
        /// <param name="stateList">A gameplay-ordered list of game states</param>
        /// <param name="isWinner">If the given state list are winning or losing states</param>
        private void UpdateTableFromStateList(IList<GameState> stateList, bool isWinner)
        {
            Assert.IsNotNull(stateList);
            Assert.IsTrue(stateList.Count > EmptyListLength);

            for (int currentStateIndex = LoopStartIndex; currentStateIndex < stateList.Count; currentStateIndex++)
            {
                double currentStateScore = CalculateStateScore(currentStateIndex, stateList.Count, isWinner);
                GameState currentState = stateList[currentStateIndex];
                if (mTable.ContainsKey(currentState))
                {
                    UpdateAverageStateScore(currentState, currentStateScore);
                }
                else
                {
                    mTable.Add(currentState, new ScoreTableData(currentStateScore));
                }
            }
        }

        private double CalculateStateScore(int index, int total, bool isWinner)
        {
            double stateScoreMagnitude = (index + 1) / (double)total;
            int stateScorePositivity = (isWinner) ? WinnerScorePositivity : LoserScorePositivity;
            return stateScoreMagnitude * stateScorePositivity;
        }

        private void UpdateAverageStateScore(GameState state, double currentStateScore)
        {
            ScoreTableData data = mTable[state];
            double scoreTotal = (data.Score * data.Frequency) + currentStateScore;
            data.Frequency++;
            data.Score = scoreTotal / (data.Frequency);
        }

        /// <summary>
        /// Returns the score associated with the given state.
        /// </summary>
        /// <param name="state">The game state to get the score for</param>
        /// <returns>The score associated with the given state</returns>
        public double GetScoreForState(GameState state)
        {
            return (mTable.ContainsKey(state)) ? mTable[state].Score : UnknownStateScore;
        }

        public override string ToString()
        {
            string returnString = "";
            foreach (KeyValuePair<GameState, ScoreTableData> tableRow in mTable)
            {
                returnString += String.Format(DisplayFormatString, tableRow.Key, tableRow.Value);
            }
            return returnString;
        }
    }
}
