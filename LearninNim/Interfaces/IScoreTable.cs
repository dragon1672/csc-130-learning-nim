﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearninNim.Players;

namespace LearninNim.Interfaces
{
    public interface IScoreTable
    {
        void UpdateTable(Player winner, IList<GameLogData> gameLog);

        double GetScoreForState(GameState state);
    }
}
