﻿using LearninNim.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim.Interfaces
{
    public interface IGameInstance
    {
        List<GameLogData> GameLog { get; }
        GameState CurrentState { get; }
        // throws InvalidOperationException
        Player Winner { get; }

        //private bool MakeMove(Tuple<int, int> moveData);

        void GameLoop();
    }
}
