﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim.Interfaces
{
    public struct GameState
    {
		private static readonly int[] defaultValues = { 3, 5, 7 };
        int[] mData;
        public int[] Data
        {
            get
            {
                if (mData == null)
                {
                    mData = new int[defaultValues.Length];
					defaultValues.CopyTo(mData,0);
                }
                return mData;
            }
            set
            {
                mData = value;
            }
        }

        public GameState(int[] values)
        {
            mData = new int[values.Length];
            values.CopyTo(mData, 0);
        }

        public override bool Equals(object obj)
        {
            bool isEqual = false;
            if (obj is GameState)
            {
                isEqual = true;
                var otherState = (GameState)obj;

                int[] myData = createCopyArray(Data);
                int[] otherData = createCopyArray(otherState.Data);

                for (int i = 0; i < myData.Length && isEqual; i++)
                {
                    isEqual = isEqual && myData[i] == otherData[i];
                }
            }
            return isEqual;
        }

        public override int GetHashCode()
        {
            int[] myData = createCopyArray(Data);

            int hash = 1;
            hash *= 17 + myData[0].GetHashCode();
            hash *= 31 + myData[1].GetHashCode();
            hash *= 13 + myData[2].GetHashCode();
            return hash;
        }

        private int[] createCopyArray(int[] dataToCopy)
        {
            int size = dataToCopy.Length;
            int[] data = new int[size];
            Array.Copy(dataToCopy, data, size);
            Array.Sort(data);

            return data;
        }

        public string ToStickString()
        {
            string ret = "";
            char stick = '|';
            for (int i = 0; i < Data.Length; i++)
            {
                ret += Data[i] + " - "; ret += new string(stick, Data[i]); ret += "\n";
            }
            return ret;
        }
        public override string ToString()
        {
            return "{" + Data[0] + "," + Data[1] + "," + Data[2] + "}";
        }

		/// <summary>
		/// Applys the passed move to the stored state
		/// note this does not contain game logic
		/// </summary>
		/// <param name="move">move to be applied</param>
        public void ApplyMove(MoveData move)
        {
            Data[move.Row] -= move.Amount;
        }

		public static GameState Move2State(GameState currentState, MoveData move)
        {
            GameState ret = new GameState();
            Array.Copy(currentState.Data, ret.Data, currentState.Data.Length);
            //ret.Data = currentState.Data;
            ret.ApplyMove(move);
            return ret;
        }
		public GameState Move2State(MoveData move)
        {
            return Move2State(this, move);
        }

		/// <summary>
		/// checks if all rows are equal to 0
		/// </summary>
		/// <param name="instance">state to check</param>
		/// <returns>if all rows in data are 0</returns>
		public static bool rowsAreEmpty(GameState instance)
		{
			bool hasData = false;
			for (int i = 0; i < instance.Data.Length && !hasData; i++)
			{
				hasData = instance.Data[i] != 0;
			}
			return !hasData;
		}
		/// <summary>
		/// checks if all rows are equal to 0
		/// </summary>
		/// <returns>if all rows in data are 0</returns>
		public bool rowsAreEmpty()
		{
			return rowsAreEmpty(this);
		}

		public static IEnumerable<MoveData> getPossibleMoves(GameState current)
		{
			for (int i = 0; i < current.Data.Length; i++)
			{
				for (int j = 1; j <= current.Data[i]; j++)
				{
					yield return new MoveData(i, j);
				}
			}
		}
    }
}
