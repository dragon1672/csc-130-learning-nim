﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim
{
	public struct MoveData
	{
		public int Row { get; private set; }
		public int Amount { get; private set; }

		public MoveData(int row, int amount) : this()
		{
			Row = row;
			Amount = amount;
		}

		public bool Equals(MoveData that)
		{
			return (this.Row == that.Row) && (this.Amount == that.Amount);
		}
		public override bool Equals(Object obj)
		{
			if (!(obj is MoveData))
			{
				return false;
			}

			MoveData that = (MoveData)obj;
			return this.Equals(that);
		}
		public static bool operator ==(MoveData a, MoveData b)
		{
			return a.Equals(b);
		}
		public static bool operator !=(MoveData a, MoveData b)
		{
			return !a.Equals(b);
		}
		public override int GetHashCode()
		{
			const int p1 = 541;
			const int p2 = 15533663;
			return Row*p1*Amount*p2;
		}
	}
}
