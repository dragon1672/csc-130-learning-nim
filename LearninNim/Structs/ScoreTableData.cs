﻿using LearninNim.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim
{
    public class ScoreTableData
    {
        private const String DataFormatString = "{0,7} | {1}";
        private const String ScoreFormatString = "N4";

        public double Score { get; set; }
        public int Frequency { get; set; }

        public ScoreTableData(double newScore)
        {
            Score = newScore;
            Frequency = 1;
        }

        public override string ToString()
        {
            return string.Format(DataFormatString, Score.ToString(ScoreFormatString), Frequency);
        }
    }
}
