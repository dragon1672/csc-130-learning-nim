﻿using LearninNim.Interfaces;
using LearninNim.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim
{
    public struct GameLogData 
    {
        public Player Player { get; set; }
        public GameState Move { get; set; }

        public GameLogData(Player player, GameState move) : this()
        {
            Player = player;
            Move = move;
        }
    }
}
