﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearninNim.Interfaces;
using LearninNim.Players;

namespace LearninNim
{
	class GameManager
	{
        enum GameOptions { PvP, PvC, SCvSC, SCvDC, Train, PrintData, None };

		static Dictionary<GameOptions, Action> playStyles =
				new Dictionary<GameOptions, Action> {
					{ GameOptions.PvP, () => setPlayers("Player 1",new Human(),"Player 2", new Human()) },
					{ GameOptions.PvC, () => setPlayers("Human", new Human(),"Computer",new AI(theScoreTable)) },
					{ GameOptions.SCvSC, () => setMulitpleGames(new AI(theScoreTable), new AI(theScoreTable)) },
					{ GameOptions.SCvDC, () => setMulitpleGames(new AI(theScoreTable), new AIRandom()) },
					{ GameOptions.Train, () => TrainAISession(new AI(theScoreTable),new AI(theScoreTable), new AIRandom())}, 
                    { GameOptions.PrintData, () => PrintScoreTableData(true)}
				};

		static Random rand = new Random();
		static IScoreTable theScoreTable;
		static Player playerOne;
        static Player playerTwo;

		static int numberOfGames;

		static void Main(string[] args)
		{
			theScoreTable = new ScoreTable();
            GameOptions playStyle;
             bool playGame = true;
			do
			{
                Console.Clear();
				Menu();
				playStyle = selectPlayStyle();
				playGame = setGameConfigs(playStyle);
                if (PlayerHasChosenToPlayAGame(playGame, playStyle)) 
				{
					startGameLoop();
				}
			} while (playGame);
		}

        static bool PlayerHasChosenToPlayAGame(bool playGame, GameOptions playStyle)
        {
            return playGame && playStyle != GameOptions.Train && playStyle != GameOptions.PrintData;
        }

		static GameOptions selectPlayStyle()
		{
            GameOptions playStyle = GameOptions.None;
			string initialInput;
            int finalInput;
			bool playStyleChosen = false;

			while (!playStyleChosen)
			{
				Console.Write("Your selection: ");
				initialInput = Console.ReadLine();
				playStyleChosen = Int32.TryParse(initialInput, out finalInput)
									&& PlayStyleCorrectlyChosen(finalInput);

				if (playStyleChosen)
				{
                    playStyle = (GameOptions)(finalInput - 1); 
				}
                else
                {
                    Console.WriteLine("\nPlease input a number between 1 and 7.");
                }
			}
            return playStyle;
		}

        static bool PlayStyleCorrectlyChosen(int input)
        {
            return input > (int)GameOptions.PvP && input <= (int)GameOptions.None + 1;
        }

		static void Menu()
		{
			Console.WriteLine("Choose a play style:\n");
			Console.WriteLine("1. Player Vs. Player\n");
			Console.WriteLine("2. Player Vs. Computer\n");
			Console.WriteLine("3. Smart Computer Vs. Smart Computer\n");
			Console.WriteLine("4. Smart Computer Vs. Dumb Computer\n");
			Console.WriteLine("5. Auto Train AI\n");
            Console.WriteLine("6. Print Data Table\n");
			Console.WriteLine("7. Quit\n");
		}

		static bool setGameConfigs(GameOptions playStyle)
		{
            bool playTheGame;
        	numberOfGames = 1;
			if (playStyles.ContainsKey(playStyle))
			{
				playStyles[playStyle]();
                playTheGame = true;
			}
			else
			{
				playTheGame = false;
			}

			Console.WriteLine("\n");
            return playTheGame;
		}

		static string promptForString(string preceedingText = "")
		{
			Console.Write(preceedingText);
			string text = Console.ReadLine();
			return text;
		}

		static string promptForPlayerName(string title)
		{
			Console.Write("\n");
			return promptForString("Set " + title + " name -> ");
		}

		static void setPlayers(string p1Title, Player p1, string p2Title, Player p2)
		{
			string p1TName = promptForPlayerName(p1Title);
			string p2TName = promptForPlayerName(p2Title);
			initPlayers(p1TName, p1, p2TName, p2);
		}

		static void initPlayers(string p1Name, Player p1, string p2Name, Player p2)
		{
			p1.Name = p1Name;
			p2.Name = p2Name;
			int randomNumber = rand.Next(0, 100);
			if (randomNumber % 2 == 0)
			{
				playerOne = p1;
				playerTwo = p2;
			}
			else
			{
				playerOne = p2;
				playerTwo = p1;
			}
		}

		static void setMulitpleGames(Player p1, Player p2)
		{
			initPlayers("AutoBot", p1, "Decepticon", p2);
			setAiGamesToPlay();
		}

		static void setAiGamesToPlay()
		{
            const int minGames = 1;
            const int maxGames = 10;

			Console.WriteLine("\nHow many games should the computers play?("+minGames+"-"+maxGames+")");
			string initialInput;
			bool numOfGamesPicked = false;

			while (!numOfGamesPicked)
			{
				initialInput = Console.ReadLine();
                numOfGamesPicked = numberOfGamesCorrectlyPicked(initialInput, minGames, maxGames);

				if (!numOfGamesPicked)
				{
                    Console.WriteLine("\nInvalid input, please input a number between " + minGames + " and " + maxGames + " for the computer to play.");
				}
			}
		}

        static bool numberOfGamesCorrectlyPicked(string choice, int minGames, int maxGames)
        {
            return (Int32.TryParse(choice, out numberOfGames) && minGames <= numberOfGames && numberOfGames <= maxGames);
        }

		static void TrainAISession(AI aiPlayer1, AI aiPlayer2, AIRandom aiPlayer3)
		{
			TrainAI("Smart AI vs Random AI", 1000, aiPlayer1, aiPlayer3);
			TrainAI("Smart AI vs Smart AI", 100000, aiPlayer1, aiPlayer2);
			TrainAI("Smart AI vs Random AI", 100000, aiPlayer1, aiPlayer3);
			TrainAI("Smart AI vs Smart AI", 50000, aiPlayer1, aiPlayer2);
			TrainAI("Smart AI vs Random AI", 100000, aiPlayer1, aiPlayer3);
			TrainAI("Smart AI vs Smart AI", 50000, aiPlayer1, aiPlayer2);
		}

		static void TrainAI(string title, int numOfGames, AI player1, AI player2)
		{
			string trainingText = title;
			numberOfGames = numOfGames;
			playerOne = player1;
			playerTwo = player2;
            Console.WriteLine("\n\n" + trainingText + "(" + numberOfGames + ")");
            startGameLoop(false, true, false);
		}

		static void TrainAI(string title, int numOfGames, AI player1, AIRandom player2)
		{
			string trainingText = title;
			numberOfGames = numOfGames;
			playerOne = player1;
			playerTwo = player2;
			Console.WriteLine("\n\n"+trainingText + "(" + numberOfGames + ")");
            startGameLoop(false, true, false);
		}

        static void startGameLoop(bool printWinner = true, bool printProgressBar = false, bool pause = true, bool printScoreTable = false)
        {
            int progressBarLocation = Console.CursorTop;
            for (int i = 0; i < numberOfGames; i++)
            {
                if (printProgressBar)
                {
                    PrintProgressBar(progressBarLocation, i, numberOfGames, 30);
                }

                IGameInstance myGame = new GameInstance(playerOne, playerTwo);
                myGame.GameLoop();
                theScoreTable.UpdateTable(myGame.Winner, myGame.GameLog);

                if (printWinner)
                {
                    Console.Clear();
                    Console.WriteLine("Winner: " + myGame.Winner.Name);
                    if (pause)
                    {
                        Console.WriteLine("\n\nPress any key to continue\nGame - "+(i+1)+"/"+numberOfGames+"\n\n");
                        Console.ReadKey(true);
                    }
                }
            }
            if (printScoreTable)
            {
               PrintScoreTableData(pause);
            }
        }

        private static string messageInABox(string src)
        {
            string ret = "+" + new string('-',src.Length) + "+\n";
            ret += "|" + src + "|\n";
            ret += "+" + new string('-',src.Length) + "+";
            return ret;
        }

		static bool updateProgressBar(int iteration, int maxIterator, int barLength)
		{
			return (barLength > maxIterator)
				|| (iteration % (maxIterator / barLength) == 0) // mid bar
				|| iteration == maxIterator-1 // bar is complete
				|| iteration == 0;
		}

		static void PrintProgressBar(int location, int iteration, int maxIterator, int barLength, bool playNoise = false)
		{
			if (updateProgressBar(iteration, maxIterator, barLength))
			{
                const int finishedFreq = 1500;
                const int finishedLength = 500;
                const int lowFreq = 500;
                const int highFreq = 1000;
                const int intervalLength = 100;

				StringBuilder progressBar = new StringBuilder();
				char head = (char)16;//'>';
				char completedHead = (char)21;//'^';
				char leftBar = (char)166;//'|';
				char rightBar = (char)166;//'|';
				char body = '-';
				char background = ' ';
				float percent = (float)iteration / (float)maxIterator;

                if (playNoise)
                {
                    int freqToPlay = (int)(percent * (highFreq - lowFreq) + lowFreq);
                    Console.Beep(freqToPlay, intervalLength);
                }

				int headPos = 0;

				progressBar.Append(leftBar);

				for (int i = 1; i < barLength - 1; i++)
				{
					float currentPercent = ((float)i + 2) / ((float)barLength);
					if (currentPercent < percent)
					{
						progressBar.Append(body);
						headPos++;
					}
					else
					{
						progressBar.Append(background);
					}
				}
				progressBar.Append(rightBar);

				if (iteration != maxIterator-1)
				{
					progressBar[headPos + 1] = head;
				}
				else
				{
					progressBar[headPos + 1] = completedHead;
                    if (playNoise)
                    {
                        Console.Beep(finishedFreq, finishedLength);
                    }
				}
                Console.SetCursorPosition(0, location);//reset to beginning of the line;
				Console.Write(progressBar);
			}
		}
        static void PrintScoreTableData(bool pause)
        {
                 Console.WriteLine("\n----- Score Data -----\n" + theScoreTable);
                if (pause)
                {
                    Console.WriteLine("\n\n" + messageInABox(" Press any key to continue "));
                    Console.ReadKey(true);
                }
        }

	}
}
