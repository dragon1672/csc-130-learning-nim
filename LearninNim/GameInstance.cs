﻿using LearninNim.Interfaces;
using LearninNim.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearninNim
{
    class GameInstance : IGameInstance
    {
        private const int maximumRow = 3;
        private const int minimumRow = 0;
        private const int noSticks = 0;
        private static readonly int[] startingBoardState = { 3, 5, 7 };

        private Player currentPlayer, nextPlayer;

        public List<GameLogData> GameLog { get; private set; }
        public GameState CurrentState { get; private set; }

        public Player Winner { get; private set; }

        public GameInstance(Player player1, Player player2)
        {
            currentPlayer = player1;
            nextPlayer = player2;

            GameLog = new List<GameLogData>();
            CurrentState = new GameState(startingBoardState);

            Winner = null;
        }

        public void GameLoop()
        {
            MoveData playersMoveData;
            while (!isGameStillGoing())
            {

                playersMoveData = currentPlayer.GetMove(this);
                if (isMoveValid(playersMoveData))
                {
                    applyMove(playersMoveData);
                    switchPlayers();
                }
            }

            Winner = currentPlayer;
            logState(); //logs state for the win
        }

        private void switchPlayers()
        {
            Player tempPlayer = currentPlayer;
            currentPlayer = nextPlayer;
            nextPlayer = tempPlayer;
        }

        void logState()
        {
            GameState logState = new GameState();
            Array.Copy(CurrentState.Data, logState.Data, CurrentState.Data.Length);
            GameLog.Add(new GameLogData(currentPlayer, logState));
        }

        void applyMove(MoveData moveData)
        {
            logState();
            CurrentState.ApplyMove(moveData);
        }

        bool isMoveValid(MoveData moveData)
        {
            int chosenRow = moveData.Row;
            int amountRemoved = moveData.Amount;

            return isRowInRange(chosenRow) && isAmountInRange(moveData);
        }

        bool isRowInRange(int chosenRow)
        {
            return (chosenRow < maximumRow) && (chosenRow >= minimumRow);
        }

        bool isAmountInRange(MoveData rowAndAmount)
        {
            return (rowAndAmount.Amount > noSticks) && (rowAndAmount.Amount <= CurrentState.Data[rowAndAmount.Row]);
        }

        bool isGameStillGoing()
        {
			return CurrentState.rowsAreEmpty();
        }
    }
}
